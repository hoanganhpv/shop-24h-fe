import { Grid } from "@mui/material";
import * as React from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

const SaleFooter = () => {
    return (
        <Grid container justifyContent={"center"} style={{ backgroundColor: "#3C3D3D" }}>
            <Grid item xl={6} lg={6} md={12} sm={12} xs={12}>
                <Grid container justifyContent={"flex-end"} alignContent={"center"}>
                    <Grid item style={{ marginLeft: "0px", marginTop: "70px", fontSize: "35px", fontWeight: "600", color: "#fff" }} xl={8} lg={8} md={8} sm={8} xs={8}>
                        Subscribe for a <span style={{ color: "#FBB710" }}>25% Discount</span>
                    </Grid>
                    <Grid item style={{ marginLeft: "0px", marginBottom: "70px", color: "#7B7C7D", fontWeight: "500" }} xl={8} lg={8} md={8} sm={8} xs={8}>
                        nvnwenvweuncwec weonfnwofnwoe fnweifie wfifoijf ojfwio fowfjifjfofe j fwiofojij fjiewj fjfof joifjowjf oifji jowfjfw
                    </Grid>
                </Grid>
            </Grid>
            <Grid justifyContent={"center"} alignItems={"center"} style={{ marginLeft: "0px", marginTop: "auto", marginBottom: "auto", fontSize: "35px", fontWeight: "600", color: "#fff" }} item xl={6} lg={6} md={12} sm={12} xs={12}>
                <TextField style={{ backgroundColor: "#fff", width: "50%" }} id="filled-basic" label="Email" variant="filled" />
                <Button style={{ marginTop: "-6px", height: "55px", backgroundColor: "#FAB610", borderRadius: "none" }} variant="contained">Subscribe</Button>
            </Grid>
        </Grid>
    );
};

export default SaleFooter;