import footerlogoImage from '../../../assets/images/icon/footerLogo.png';

function FooterLogo() {
    return (
        <img className='logo-image' src={footerlogoImage}></img>
    );
};

export default FooterLogo;