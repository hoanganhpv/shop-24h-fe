import { Grid } from "@mui/material";
import * as React from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import FooterLogo from "./FooterLogo.js";

const ContactFooter = () => {
    return (
        <>
            <Grid container justifyContent={"center"} style={{ backgroundColor: "#242524" }}>
                <Grid item xl={6} lg={6} md={12} sm={12} xs={12}>
                    <Grid container justifyContent={"flex-end"} alignContent={"center"}>
                        <Grid item style={{ marginLeft: "0px", marginTop: "70px", fontSize: "35px", fontWeight: "600", color: "#fff" }} xl={8} lg={8} md={8} sm={8} xs={8}>
                            <FooterLogo />
                        </Grid>
                        <Grid item style={{ marginLeft: "0px", marginBottom: "70px", color: "#7B7C7D", fontWeight: "500", fontSize: "12px", marginTop: "20px" }} xl={8} lg={8} md={8} sm={8} xs={8}>
                            Copyright ©2023 All rights reserved | This template is made with  by Colorlib & Re-distributed by Themewagon
                        </Grid>
                    </Grid>
                </Grid>
                <Grid justifyContent={"center"} alignItems={"center"} style={{ marginLeft: "0px", marginTop: "auto", marginBottom: "auto", fontSize: "35px", fontWeight: "600", color: "#fff" }} item xl={6} lg={6} md={12} sm={12} xs={12}>
                    <Grid style={{width: "70%", margin: "auto"}} container justifyContent={"space-around"}>
                        <Grid item style={{textAlign: "center"}}>
                            <a href="#" style={{fontSize: "15px", textDecoration: "none", textAlign: "center", color: "#fff"}}>HOME</a>
                        </Grid>
                        <Grid item style={{textAlign: "center"}}>
                            <a href="#" style={{fontSize: "15px", textDecoration: "none", textAlign: "center", color: "#fff"}}>SHOP</a>
                        </Grid>
                        <Grid item style={{textAlign: "center"}}>
                            <a href="#" style={{fontSize: "15px", textDecoration: "none", textAlign: "center", color: "#fff"}}>PRODUCT</a>
                        </Grid>
                        <Grid item style={{textAlign: "center"}}>
                            <a href="#" style={{fontSize: "15px", textDecoration: "none", textAlign: "center", color: "#fff"}}>CART</a>
                        </Grid>
                        <Grid item style={{textAlign: "center"}}>
                            <a href="#" style={{fontSize: "15px", textDecoration: "none", textAlign: "center", color: "#fff"}}>CHECKOUT</a>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
};

export default ContactFooter;