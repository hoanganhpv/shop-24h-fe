import { Grid } from "@mui/material";
import mainImage1 from '../../../assets/images/main-products/1.jpg';
import mainImage2 from '../../../assets/images/main-products/2.jpg';
import mainImage3 from '../../../assets/images/main-products/3.jpg';
import mainImage4 from '../../../assets/images/main-products/4.jpg';
import mainImage5 from '../../../assets/images/main-products/5.jpg';
import mainImage6 from '../../../assets/images/main-products/6.jpg';
import mainImage7 from '../../../assets/images/main-products/7.jpg';
import mainImage8 from '../../../assets/images/main-products/8.jpg';
import mainImage9 from '../../../assets/images/main-products/9.jpg';

const HomePage = () => {
    return (
            <Grid style={{ margin: "0px", padding: "0px" }} container>
                <Grid item xl={4} lg={4} md={6} sm={12} xs={12}>
                    <Grid item style={{ position: "relative" }}>
                        <Grid className="main-item-image" style={{ width: "100%", height: "100%", margin: "0px", padding: "10% 0px 0px 10%", display: "inline-block", position: "absolute", top: "0", left: "0" }}>
                            <Grid item xl={8} lg={8} md={8} sm={8} xs={8}>
                                <hr style={{ backgroundColor: "rgb(251,183,16)", height: "5px", width: "100px", border: "none", boxShadow: "none" }}></hr>
                            </Grid>
                            <Grid style={{ fontSize: "20px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                From $180
                            </Grid>
                            <Grid style={{ fontSize: "35px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                Modern Chair
                            </Grid>
                        </Grid>
                        <img className="main-image" src={mainImage1} width={"100%"} alt="main-image-1"></img>
                    </Grid>
                    <Grid item style={{ position: "relative" }}>
                        <Grid className="main-item-image" style={{ width: "100%", height: "100%", margin: "0px", padding: "10% 0px 0px 10%", display: "inline-block", position: "absolute", top: "0", left: "0" }}>
                            <Grid item xl={8} lg={8} md={8} sm={8} xs={8}>
                                <hr style={{ backgroundColor: "rgb(251,183,16)", height: "5px", width: "100px", border: "none", boxShadow: "none" }}></hr>
                            </Grid>
                            <Grid style={{ fontSize: "20px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                From $180
                            </Grid>
                            <Grid style={{ fontSize: "35px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                Minimalistic Plan Pot
                            </Grid>
                        </Grid>
                        <img className="main-image" src={mainImage5} width={"100%"} alt="main-image-5"></img>
                    </Grid>
                    <Grid item style={{ position: "relative" }}>
                        <Grid className="main-item-image" style={{ width: "100%", height: "100%", margin: "0px", padding: "10% 0px 0px 10%", display: "inline-block", position: "absolute", top: "0", left: "0" }}>
                            <Grid item xl={8} lg={8} md={8} sm={8} xs={8}>
                                <hr style={{ backgroundColor: "rgb(251,183,16)", height: "5px", width: "100px", border: "none", boxShadow: "none" }}></hr>
                            </Grid>
                            <Grid style={{ fontSize: "20px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                From $180
                            </Grid>
                            <Grid style={{ fontSize: "35px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                Modern Chair
                            </Grid>
                        </Grid>
                        <img className="main-image" src={mainImage8} width={"100%"} alt="main-image-8"></img>
                    </Grid>
                </Grid>
                <Grid item xl={4} lg={4} md={6} sm={12} xs={12}>
                    <Grid item style={{ position: "relative" }}>
                        <Grid className="main-item-image" style={{ width: "100%", height: "100%", margin: "0px", padding: "10% 0px 0px 10%", display: "inline-block", position: "absolute", top: "0", left: "0" }}>
                            <Grid item xl={8} lg={8} md={8} sm={8} xs={8}>
                                <hr style={{ backgroundColor: "rgb(251,183,16)", height: "5px", width: "100px", border: "none", boxShadow: "none" }}></hr>
                            </Grid>
                            <Grid style={{ fontSize: "20px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                From $18
                            </Grid>
                            <Grid style={{ fontSize: "35px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                Plan Pot
                            </Grid>
                        </Grid>
                        <img className="main-image" src={mainImage2} width={"100%"} alt="main-image-2"></img>
                    </Grid>
                    <Grid item style={{ position: "relative" }}>
                        <Grid className="main-item-image" style={{ width: "100%", height: "100%", margin: "0px", padding: "10% 0px 0px 10%", display: "inline-block", position: "absolute", top: "0", left: "0" }}>
                            <Grid item xl={8} lg={8} md={8} sm={8} xs={8}>
                                <hr style={{ backgroundColor: "rgb(251,183,16)", height: "5px", width: "100px", border: "none", boxShadow: "none" }}></hr>
                            </Grid>
                            <Grid style={{ fontSize: "20px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                From $320
                            </Grid>
                            <Grid style={{ fontSize: "35px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                Small Table
                            </Grid>
                        </Grid>
                        <img className="main-image" src={mainImage6} width={"100%"} alt="main-image-6"></img>
                    </Grid>
                    <Grid item style={{ position: "relative" }}>
                        <Grid className="main-item-image" style={{ width: "100%", height: "100%", margin: "0px", padding: "10% 0px 0px 10%", display: "inline-block", position: "absolute", top: "0", left: "0" }}>
                            <Grid item xl={8} lg={8} md={8} sm={8} xs={8}>
                                <hr style={{ backgroundColor: "rgb(251,183,16)", height: "5px", width: "100px", border: "none", boxShadow: "none" }}></hr>
                            </Grid>
                            <Grid style={{ fontSize: "20px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                From $180
                            </Grid>
                            <Grid style={{ fontSize: "35px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                Night Stand
                            </Grid>
                        </Grid>
                        <img className="main-image" src={mainImage9} width={"100%"} alt="main-image-9"></img>
                    </Grid>
                </Grid>
                <Grid item xl={4} lg={4} md={12} sm={12} xs={12}>
                    <Grid item style={{ position: "relative" }}>
                        <Grid className="main-item-image" style={{ width: "100%", height: "100%", margin: "0px", padding: "10% 0px 0px 10%", display: "inline-block", position: "absolute", top: "0", left: "0" }}>
                            <Grid item xl={8} lg={8} md={8} sm={8} xs={8}>
                                <hr style={{ backgroundColor: "rgb(251,183,16)", height: "5px", width: "100px", border: "none", boxShadow: "none" }}></hr>
                            </Grid>
                            <Grid style={{ fontSize: "20px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                From $318
                            </Grid>
                            <Grid style={{ fontSize: "35px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                Modern Rocking Chair
                            </Grid>
                        </Grid>
                        <img className="main-image" src={mainImage3} width={"100%"} alt="main-image-3"></img>
                    </Grid>
                    <Grid item style={{ position: "relative" }}>
                        <Grid className="main-item-image" style={{ width: "100%", height: "100%", margin: "0px", padding: "10% 0px 0px 10%", display: "inline-block", position: "absolute", top: "0", left: "0" }}>
                            <Grid item xl={8} lg={8} md={8} sm={8} xs={8}>
                                <hr style={{ backgroundColor: "rgb(251,183,16)", height: "5px", width: "100px", border: "none", boxShadow: "none" }}></hr>
                            </Grid>
                            <Grid style={{ fontSize: "20px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                From $318
                            </Grid>
                            <Grid style={{ fontSize: "35px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                Home Deco
                            </Grid>
                        </Grid>
                        <img className="main-image" src={mainImage4} width={"100%"} alt="main-image-4"></img>
                    </Grid>
                    <Grid item style={{ position: "relative" }}>
                        <Grid className="main-item-image" style={{ width: "100%", height: "100%", margin: "0px", padding: "10% 0px 0px 10%", display: "inline-block", position: "absolute", top: "0", left: "0" }}>
                            <Grid item xl={8} lg={8} md={8} sm={8} xs={8}>
                                <hr style={{ backgroundColor: "rgb(251,183,16)", height: "5px", width: "100px", border: "none", boxShadow: "none" }}></hr>
                            </Grid>
                            <Grid style={{ fontSize: "20px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                From $318
                            </Grid>
                            <Grid style={{ fontSize: "35px", fontWeight: "600", color: "#6C6C88" }} item xl={8} lg={8} md={8} sm={8} xs={8}>
                                Metallic Chair
                            </Grid>
                        </Grid>
                        <img className="main-image" src={mainImage7} width={"100%"} alt="main-image-7"></img>
                    </Grid>
                </Grid>
            </Grid>
    );
};

export default HomePage;