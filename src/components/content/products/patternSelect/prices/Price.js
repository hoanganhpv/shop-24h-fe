import * as React from 'react';
import { Grid, Typography } from "@mui/material";
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';

function valuetext(value) {
    return `${value}°C`;
}

const Prices = () => {
    const [value, setValue] = React.useState([10, 1000]);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Grid container style={{ margin: "0px", padding: "0px" }}>
            <Grid style={{ margin: "0px 5px 0px" }} item xl={12} lg={12} md={12} sm={12} xs={12}>
                <Typography style={{ width: "100%", fontSize: "15px", fontWeight: "600", color: "#252425" }}>Price</Typography>
            </Grid>
            <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ marginTop: "15%" }}>
                <Box sx={{ width: "100%" }}>
                    <Slider
                        getAriaLabel={() => 'Temperature range'}
                        value={value}
                        onChange={handleChange}
                        valueLabelDisplay="auto"
                        getAriaValueText={valuetext}
                    />
                </Box>
                <Grid style={{ marginTop: "10%" }} item xl={12} lg={12} md={12} sm={12} xs={12}>
                    <Typography style={{ fontSize: "15px", fontWeight: "600", color: "#959595" }}>$10 - $1000</Typography>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Prices;