import { Grid, Typography } from "@mui/material";

const Categories = () => {
    return (
        <Grid container style={{ margin: "0px", padding: "0px" }}>
            <Grid style={{ margin: "0px 5px 0px" }} item xl={12} lg={12} md={12} sm={12} xs={12}>
                <Typography style={{ width: "100%", fontSize: "15px", fontWeight: "600", color: "#252425" }}>Categories</Typography>
            </Grid>
            <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                <ul style={{ marginTop: "30px" }}>
                    <li className="categories-list-item">Chairs</li>
                    <li className="categories-list-item">Bed</li>
                    <li className="categories-list-item">Accesories</li>
                    <li className="categories-list-item">Furniture</li>
                    <li className="categories-list-item">Home Deco</li>
                    <li className="categories-list-item">Dressings</li>
                    <li className="categories-list-item">Tables</li>

                </ul>
            </Grid>
        </Grid>
    );
};

export default Categories;