import * as React from 'react';
import Checkbox from '@mui/material/Checkbox';
import { Grid, Typography } from "@mui/material";

const Brands = () => {
    return (
        <Grid container style={{ margin: "0px", padding: "0px" }}>
            <Grid style={{ margin: "0px 5px 0px" }} item xl={12} lg={12} md={12} sm={12} xs={12}>
                <Typography style={{ width: "100%", fontSize: "15px", fontWeight: "600", color: "#252425" }}>Brands</Typography>
            </Grid>
            <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                <ul style={{ marginTop: "30px", paddingLeft: "1%" }}>
                    <li className="brands-list-item">
                        <Checkbox id='amado-brand' /><label for="amado-brand">Amado</label>
                    </li>
                    <li className="brands-list-item">
                        <Checkbox id='ikea-brand' /><label for="ikea-brand">Ikea</label>
                    </li>
                    <li className="brands-list-item">
                        <Checkbox id='funiture-inc-brand' /><label for="funiture-inc-brand">Funiture Inc</label>
                    </li>
                    <li className="brands-list-item">
                        <Checkbox id='the-factory-brand' /><label for="the-factory-brand">The Factory</label>
                    </li>
                    <li className="brands-list-item">
                        <Checkbox id='artdeco-brand' /><label for="artdeco-brand">Artdeco</label>
                    </li>

                </ul>
            </Grid>
        </Grid>
    );
};

export default Brands;