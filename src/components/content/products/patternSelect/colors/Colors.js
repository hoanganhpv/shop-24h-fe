import * as React from 'react';
import { Grid, Typography, Box } from "@mui/material";

const Colors = () => {
    return (
        <>
            <Grid container style={{ margin: "0px", padding: "0px" }}>
                <Grid style={{ margin: "0px 5px 0px" }} item xl={12} lg={12} md={12} sm={12} xs={12}>
                    <Typography style={{ width: "100%", fontSize: "15px", fontWeight: "600", color: "#252425" }}>Colors</Typography>
                </Grid>
                <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ marginTop: "10%" }}>
                    <Grid container style={{ margin: "0px", padding: "0px" }}>
                        <Grid style={{ padding: "3%" }} item xl={3} lg={3} md={6} sm={6} xs={12}>
                            <Box style={{ borderRadius: "50%", backgroundColor: "#FFFEFE", height: "30px", width: "30px" }}></Box>
                        </Grid>
                        <Grid style={{ padding: "3%" }} item xl={3} lg={3} md={6} sm={6} xs={12}>
                            <Box style={{ borderRadius: "50%", backgroundColor: "#969696", height: "30px", width: "30px" }}></Box>
                        </Grid>
                        <Grid style={{ padding: "3%" }} item xl={3} lg={3} md={6} sm={6} xs={12}>
                            <Box style={{ borderRadius: "50%", backgroundColor: "#030202", height: "30px", width: "30px" }}></Box>
                        </Grid>
                        <Grid style={{ padding: "3%" }} item xl={3} lg={3} md={6} sm={6} xs={12}>
                            <Box style={{ borderRadius: "50%", backgroundColor: "#0215FE", height: "30px", width: "30px" }}></Box>
                        </Grid>
                        <Grid style={{ padding: "3%" }} item xl={3} lg={3} md={6} sm={6} xs={12}>
                            <Box style={{ borderRadius: "50%", backgroundColor: "#DC0746", height: "30px", width: "30px" }}></Box>
                        </Grid>
                        <Grid style={{ padding: "3%" }} item xl={3} lg={3} md={6} sm={6} xs={12}>
                            <Box style={{ borderRadius: "50%", backgroundColor: "#FFF46B", height: "30px", width: "30px" }}></Box>
                        </Grid>
                        <Grid style={{ padding: "3%" }} item xl={3} lg={3} md={6} sm={6} xs={12}>
                            <Box style={{ borderRadius: "50%", backgroundColor: "#F36E50", height: "30px", width: "30px" }}></Box>
                        </Grid>
                        <Grid style={{ padding: "3%" }} item xl={3} lg={3} md={6} sm={6} xs={12}>
                            <Box style={{ borderRadius: "50%", backgroundColor: "#9B8677", height: "30px", width: "30px" }}></Box>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
};

export default Colors;