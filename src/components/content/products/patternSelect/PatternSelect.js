import { Grid } from "@mui/material";
import Categories from "./categories/Categories.js";
import Brands from "./brands/Brands.js";
import Colors from "./colors/Colors.js";
import Prices from "./prices/Price.js";

const PatternSelect = () => {
    return (
        <>
            <Grid item xl={2} lg={2} md={2} sm={2} xs={2} style={{ backgroundColor: "#F4F7FB", padding: "2%", margin: "0" }}>
                <Grid container style={{ margin: "0px", padding: "0px", height: "100%" }}>
                    <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ margin: "auto 0px", padding: "0px" }} justifyContent={"center"}>
                        <Categories />
                    </Grid>
                    <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ margin: "auto 0px", padding: "0px" }} justifyContent={"center"}>
                        <Brands />
                    </Grid>
                    <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ margin: "auto 0px", padding: "0px" }} justifyContent={"center"}>
                        <Colors />
                    </Grid>
                    <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ margin: "auto 0px", padding: "0px" }} justifyContent={"center"}>
                        <Prices />
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
};

export default PatternSelect	