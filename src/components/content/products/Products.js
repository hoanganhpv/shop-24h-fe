import PatternSelect from "./patternSelect/PatternSelect.js";
import { Grid } from "@mui/material";

const Products = () => {
    return (
        <>
            <Grid item xl={9} lg={9} md={12} sm={12} xs={12} sx={{ display: "flex" }}>
                <Grid container>
                    <PatternSelect />
                    <Grid item xl={9} lg={9} md={9} sm={9} xs={9}></Grid>
                </Grid>
            </Grid>
        </>
    );
};

export default Products;