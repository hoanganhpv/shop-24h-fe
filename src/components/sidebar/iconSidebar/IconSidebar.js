import React from "react";
import { Container } from "reactstrap";
import NavigationSideBar from "./NavigationSideBar.js";
import FavoriteSideBar from "./FavoriteSideBar.js";
import ButtonSideBar from "./ButtonSideBar.js";
import Contact from "./Contact.js";
import Logo from "./Logo.js"

function IconSidebar() {
    return (
        <Container className="main-sidebar sidebar ml-0">
            {/* LOGO */}
            <Logo />
            {/* Sidebar chia layout 4 phần 
            sessison 1: item ul(5 li);
            sessison 2: 2 button (discount + new this week);
            sessison 3: (cart + favorite + search)
            sessison 4: (contact item)
        */}
            {/* *** START Sessison 1 *** */}
            <NavigationSideBar />
            {/* ### END Sessison 1 ### */}

            {/* *** START Sessison 2 *** */}
            <ButtonSideBar />
            {/* ### END Sessison 2 ### */}

            {/* *** START Sessison 3 *** */}
            <FavoriteSideBar />
            {/* ### END Sessison 3 ### */}

            {/* *** START Sessison 4 *** */}
            <Contact />
            {/* ### END Sessison 4 ### */}
        </Container>
    )
};

export default IconSidebar;