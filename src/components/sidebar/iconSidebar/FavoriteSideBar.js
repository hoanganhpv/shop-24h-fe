import React from "react";
import { Row } from "reactstrap";
import cartIcon from '../../../assets/images/icon/cart.png';
import favoriteIcon from '../../../assets/images/icon/favorites.png';
import search from '../../../assets/images/icon/search.png';

const FavoriteSideBar = () => {
    return (
        <>
            {/* CART */}
            <Row style={{ width: "170px", marginTop: "100px" }}>
                <a style={{ color: "#131228", fontSize: "17px", fontWeight: "600" }} className="option-sidebar" href="#"><img src={cartIcon} />&nbsp;&nbsp;CART(<span>0</span>)</a>
            </Row>
            {/* FAVORITE */}
            <Row style={{ width: "170px" }}>
                <a style={{ color: "#131228", fontSize: "17px", fontWeight: "600" }} className="option-sidebar" href="#"><img src={favoriteIcon} />&nbsp;&nbsp;FAVORITE</a>
            </Row>
            {/* SEARCH */}
            <Row style={{ width: "170px" }}>
                <a style={{ color: "#131228", fontSize: "17px", fontWeight: "600" }} className="option-sidebar" href="#"><img src={search} />&nbsp;&nbsp;SEARCH</a>
            </Row>
        </>
    );
};

export default FavoriteSideBar;