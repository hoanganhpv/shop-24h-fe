import React from "react";
import { Grid } from "@mui/material";
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';

const Contact = () => {
    return (
        <Grid justifyContent={"space-between"} style={{ width: "100%", marginTop: "200px", color: "#C8C4C1" }} container>
            <Grid justifyContent={"center"} style={{ textAlign: "center" }} item xl={4}>
                <FacebookIcon />
            </Grid>
            <Grid justifyContent={"center"} style={{ textAlign: "center" }} item xl={4}>
                <InstagramIcon />
            </Grid>
            <Grid justifyContent={"center"} style={{ textAlign: "center" }} item xl={4}>
                <TwitterIcon />
            </Grid>
        </Grid>
    );
};

export default Contact;