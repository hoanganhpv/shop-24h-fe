import React from "react";
import { Col, Row } from "reactstrap";
import { Button } from "@mui/material";

const ButtonSideBar = () => {
    return (
        <>
            <Row style={{ marginTop: "100px" }}>
                <Col xl={12}>
                    <Button className="discount-btn"><a href="#">Discount</a></Button>
                </Col>
                <Col xl={12}>
                    <Button className="new-btn"><a href="#">New this week</a></Button>
                </Col>
            </Row>
        </>
    );
};

export default ButtonSideBar;