import IconSidebar from "./iconSidebar/IconSidebar.js";
import Logo from "./iconSidebar/Logo.js";
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import { Grid } from "@mui/material";

function Sidebar() {

    const [anchorElNav, setAnchorElNav] = React.useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const [showSidebar, setShowSidebar] = React.useState(true);

    React.useEffect(() => {
        const handleResize = () => {
            setShowSidebar(window.innerWidth >= 1200);
        };
        handleResize();
        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    }, []);

    return (
        <Grid item xl={3} lg={3} md={12} sm={12} xs={12}>
            <AppBar position="relative" sx={{
                width: '100%',
                maxWidth: showSidebar ? '500px' : '100%',
                minWidth: '280px',
                left: 0,
                display: 'flex',
                flexDirection: 'row',
                justifyContent: showSidebar ? 'flex-start' : 'space-between',
                alignItems: 'stretch',
                transition: 'max-width 0.2s ease-in-out',
                zIndex: theme => theme.zIndex.drawer + 1,
                backgroundColor: "#fff",
                boxShadow: "none"
            }}>
                {showSidebar &&
                    <Grid container>
                        <IconSidebar />
                    </Grid>
                }
                <Container maxWidth="xl" sx={{ marginLeft: showSidebar ? '500px' : '0' }}>
                    <Toolbar style={{ margin: "0px", padding: "0px" }} disableGutters>
                        <Box justifyContent={"space-between"} style={{ margin: "0", padding: "0" }} sx={{ flexGrow: 1, display: { md: 'flex', lg: 'none' } }} >
                            <Grid style={{ margin: "0", padding: "0" }} justifyContent={"space-between"} container>
                                <Grid justifyContent={"center"} item xl={12} lg={12} md={7} sm={7} xs={7}>
                                    <IconButton
                                        size="large"
                                        aria-label="account of current user"
                                        aria-controls="menu-appbar"
                                        aria-haspopup="true"
                                        onClick={handleOpenNavMenu}
                                        color="#C8C4C1"
                                    >
                                        <MenuIcon style={{ fontSize: "50px" }} />
                                    </IconButton>

                                </Grid>
                                <Grid justifyContent={"center"} style={{ padding: "auto", margin: "auto" }} item xl={12} lg={12} md={3} sm={3} xs={3}>
                                    <Logo />
                                </Grid>
                            </Grid>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorElNav}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                                open={Boolean(anchorElNav)}
                                onClose={handleCloseNavMenu}
                                sx={{
                                    display: { md: 'block', lg: 'none' },
                                }}
                            >
                                <IconSidebar />
                            </Menu>
                        </Box>
                    </Toolbar>
                </Container>
            </AppBar>
        </Grid>
    );
};

export default Sidebar;